mod common;

#[cfg(any(feature = "bsp_virt"))]
mod virt;

#[cfg(any(feature = "bsp_virt"))]
pub use virt::*;
