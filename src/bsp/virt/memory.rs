//! BSP Memory Management.

//--------------------------------------------------------------------------------------------------
// Public Definitions
//--------------------------------------------------------------------------------------------------

/// The board's physical memory map.
#[rustfmt::skip]
pub(super) mod map {
    pub const UART_OFFSET:         usize = 0x09_000_000;

    /// Physical devices.
    #[cfg(feature = "bsp_virt")]
    pub mod mmio {
        use super::*;

        pub const START     : usize =         0x0;
        pub const UART_START: usize = START + UART_OFFSET;
    }
}
