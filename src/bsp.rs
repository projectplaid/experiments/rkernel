mod device_driver;

#[cfg(any(feature = "bsp_rpi3", feature = "bsp_rpi4"))]
mod raspberrypi;

#[cfg(any(feature = "bsp_rpi3", feature = "bsp_rpi4"))]
pub use raspberrypi::*;

#[cfg(any(feature = "bsp_virt"))]
mod virt;

#[cfg(any(feature = "bsp_virt"))]
pub use virt::*;
